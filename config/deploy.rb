lock '3.4.0'

set :application, 'deploy-example'
set :repo_url, 'git@bitbucket.org:sketchthat/deploy-example.git'
set :branch, 'master'
set :deploy_to, '/var/www/deploy-example'

set :linked_files, fetch(:linked_files, []).push('app/config/parameters.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('vendor', 'var')

set :composer_install_flags, '--no-interaction --optimize-autoloader'
set :symfony_parameters_upload, :never

after 'deploy:starting', 'composer:install_executable'
#after 'deploy:updated', 'symfony:assets:install'
after 'deploy:symlink:release', 'symfony_custom:database'

set :permission_method, :acl
set :file_permissions_users, ["www-data"]
set :file_permissions_paths, ["var"]