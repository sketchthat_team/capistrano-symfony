# Ruby & Capistrano Setup

Setup instructions for Capistrano to work with Symfony.

## Installation

### Ruby Setup
    $ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    $ curl -L https://get.rvm.io | bash -s stable --rails
    $ source ~/.rvm/scripts/rvm
    $ bundle init

`Gemfile` requires two gems.

```ruby
gem 'capistrano', '~> 3.4.0'
gem 'capistrano-symfony', '~> 1.0.0.rc1'
```

    $ bundle install

### Capistrano Setup

Now we're ready to install and configure Capistrano.

    $ cap install

Update the `Capfile` with the Symfony plugin.

```ruby
require 'capistrano/symfony'
```

### Capistrano Configuration

Update `config/deploy.rb`

```ruby
set :application, 'project-name'
set :repo_url, 'git@bitbucket.org:sketchthat/project-name.git'
set :branch, 'master'
set :deploy_to, '/var/www/project-folder'
```

The `config/deploy/staging.rb` and `config/deploy/production.rd` overwrite the `config/deploy.rb` options.

Update the files with the correct configuration for folders, branches and roles

```ruby
role :web, %w{user@my-server.com}
role :app, %w{user@my-server.com}
```

### Deployment

@todo
First deployment will fail as we don't have the shared `parameters.yml` setup & the file permissions of `var` might be wrong.

    $ # Production / Staging Server
    $ sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX var
    $ sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var


    $ # Development Machine
    $ cap staging deploy
    $ cap production deploy